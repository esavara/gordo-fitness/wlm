"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = require("./index");
test("calcula pesos para 25 libras", function () {
    expect((0, index_1.calculateWeightWarmup)(25, 0, 25, 0)).toStrictEqual([]);
});
test("calcula pesos para 165 libras con una barra de 35 libras y 45 libras como peso minimo", function () {
    expect((0, index_1.calculateWeightWarmup)(165, 0, 35, 45)).toStrictEqual([
        {
            warmupSet: 1,
            totalWeight: 45,
            jump: 30,
            barWeight: 35,
            extraBarWeight: 10,
        },
        {
            warmupSet: 2,
            totalWeight: 45,
            jump: 30,
            barWeight: 35,
            extraBarWeight: 10,
        },
        {
            warmupSet: 3,
            totalWeight: 75,
            jump: 30,
            barWeight: 35,
            extraBarWeight: 10,
        },
        {
            warmupSet: 4,
            totalWeight: 105,
            jump: 30,
            barWeight: 35,
            extraBarWeight: 10,
        },
        {
            warmupSet: 5,
            totalWeight: 135,
            jump: 30,
            barWeight: 35,
            extraBarWeight: 10,
        },
    ]);
});
test("calcula pesos para 165 libras", function () {
    expect((0, index_1.calculateWeightWarmup)(165)).toStrictEqual([
        {
            warmupSet: 1,
            totalWeight: 45,
            jump: 30,
            barWeight: 45,
            extraBarWeight: 0,
        },
        {
            warmupSet: 2,
            totalWeight: 45,
            jump: 30,
            barWeight: 45,
            extraBarWeight: 0,
        },
        {
            warmupSet: 3,
            totalWeight: 75,
            jump: 30,
            barWeight: 45,
            extraBarWeight: 0,
        },
        {
            warmupSet: 4,
            totalWeight: 105,
            jump: 30,
            barWeight: 45,
            extraBarWeight: 0,
        },
        {
            warmupSet: 5,
            totalWeight: 135,
            jump: 30,
            barWeight: 45,
            extraBarWeight: 0,
        },
    ]);
});
test("calcula pesos para 105 libras", function () {
    expect((0, index_1.calculateWeightWarmup)(105)).toStrictEqual([
        {
            warmupSet: 1,
            totalWeight: 45,
            jump: 15,
            barWeight: 45,
            extraBarWeight: 0,
        },
        {
            warmupSet: 2,
            totalWeight: 45,
            jump: 15,
            barWeight: 45,
            extraBarWeight: 0,
        },
        {
            warmupSet: 3,
            totalWeight: 60,
            jump: 15,
            barWeight: 45,
            extraBarWeight: 0,
        },
        {
            warmupSet: 4,
            totalWeight: 75,
            jump: 15,
            barWeight: 45,
            extraBarWeight: 0,
        },
        {
            warmupSet: 5,
            totalWeight: 90,
            jump: 15,
            barWeight: 45,
            extraBarWeight: 0,
        },
    ]);
});
test("calcula pesos para 75 libras", function () {
    expect((0, index_1.calculateWeightWarmup)(75)).toStrictEqual([
        {
            warmupSet: 1,
            totalWeight: 45,
            jump: 10,
            barWeight: 45,
            extraBarWeight: 0,
        },
        {
            warmupSet: 2,
            totalWeight: 45,
            jump: 10,
            barWeight: 45,
            extraBarWeight: 0,
        },
        {
            warmupSet: 3,
            totalWeight: 55,
            jump: 10,
            barWeight: 45,
            extraBarWeight: 0,
        },
        {
            warmupSet: 4,
            totalWeight: 65,
            jump: 10,
            barWeight: 45,
            extraBarWeight: 0,
        },
    ]);
});
test("calcula pesos para 225 libras", function () {
    expect((0, index_1.calculateWeightWarmup)(225)).toStrictEqual([
        {
            warmupSet: 1,
            totalWeight: 45,
            jump: 40,
            barWeight: 45,
            extraBarWeight: 0,
        },
        {
            warmupSet: 2,
            totalWeight: 45,
            jump: 40,
            barWeight: 45,
            extraBarWeight: 0,
        },
        {
            warmupSet: 3,
            totalWeight: 95,
            jump: 50,
            barWeight: 45,
            extraBarWeight: 0,
        },
        {
            warmupSet: 4,
            totalWeight: 135,
            jump: 40,
            barWeight: 45,
            extraBarWeight: 0,
        },
        {
            warmupSet: 5,
            totalWeight: 185,
            jump: 50,
            barWeight: 45,
            extraBarWeight: 0,
        },
    ]);
});
test("calcula pesos para 545 libras con peso inicial de 135 libras", function () {
    expect((0, index_1.calculateWeightWarmup)(545, 135)).toStrictEqual([
        {
            warmupSet: 1,
            totalWeight: 135,
            jump: 90,
            barWeight: 45,
            extraBarWeight: 0,
        },
        {
            warmupSet: 2,
            totalWeight: 135,
            jump: 90,
            barWeight: 45,
            extraBarWeight: 0,
        },
        {
            warmupSet: 3,
            totalWeight: 225,
            jump: 90,
            barWeight: 45,
            extraBarWeight: 0,
        },
        {
            warmupSet: 4,
            totalWeight: 315,
            jump: 90,
            barWeight: 45,
            extraBarWeight: 0,
        },
        {
            warmupSet: 5,
            totalWeight: 405,
            jump: 90,
            barWeight: 45,
            extraBarWeight: 0,
        },
        {
            warmupSet: 6,
            totalWeight: 495,
            jump: 90,
            barWeight: 45,
            extraBarWeight: 0,
        },
    ]);
});
test("calcula pesos para 545 libras con carga progresiva de barra vacia, 45%, 65% y 85%", function () {
    expect((0, index_1.calculateWeightWarmupPerc)(315, [45, 65, 85], true)).toStrictEqual([
        {
            warmupSet: 1,
            totalWeight: 45,
            jump: 0,
            barWeight: 45,
            extraBarWeight: 0,
        },
        {
            warmupSet: 2,
            totalWeight: 45,
            jump: 0,
            barWeight: 45,
            extraBarWeight: 0,
        },
        {
            warmupSet: 3,
            totalWeight: 145,
            jump: 45,
            barWeight: 45,
            extraBarWeight: 0,
        },
        {
            warmupSet: 4,
            totalWeight: 205,
            jump: 65,
            barWeight: 45,
            extraBarWeight: 0,
        },
        {
            warmupSet: 5,
            totalWeight: 270,
            jump: 85,
            barWeight: 45,
            extraBarWeight: 0,
        },
    ]);
});
test("calcula los platos por lado necesarios para 90 libras", function () {
    expect((0, index_1.calculatePlatesLBS)([2.5, 45, 25], 90)).toStrictEqual([
        { plate: 45, amount: 1 },
    ]);
});
test("calcula los platos por lado necesarios para 145 libras", function () {
    expect((0, index_1.calculatePlatesLBS)([2.5, 45, 25], 145)).toStrictEqual([
        { plate: 45, amount: 1 },
        { plate: 25, amount: 1 },
        { plate: 2.5, amount: 1 },
    ]);
});
test("calcula los platos por lado necesarios para 235 libras", function () {
    expect((0, index_1.calculatePlatesLBS)([2.5, 45, 25], 235)).toStrictEqual([
        { plate: 45, amount: 2 },
        { plate: 25, amount: 1 },
        { plate: 2.5, amount: 1 },
    ]);
});
test("calcula los platos por lado necesarios para 235 libras, con peso sin contabilizar", function () {
    expect((0, index_1.calculatePlatesLBS)([45, 25], 235)).toStrictEqual([
        { plate: 45, amount: 2 },
        { plate: 25, amount: 1 },
        { plate: 5, amount: 0 },
    ]);
});
//# sourceMappingURL=math.test.js.map