export declare type WeightWarmup = Array<{
    warmupSet: number;
    totalWeight: number;
    jump: number;
    barWeight: number;
    extraBarWeight: number;
}>;
export declare type PlateLBS = {
    plate: number;
    amount: number;
};
