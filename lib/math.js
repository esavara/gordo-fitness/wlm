"use strict";
/*
 * Contiene utilidades para calcular varios aspectos en una rutina para el
 * levantamiento de pesas
 * */
Object.defineProperty(exports, "__esModule", { value: true });
exports.calculatePlatesLBS = exports.calculateWeightWarmupPerc = exports.calculateWeightWarmup = void 0;
// Convierte valores negativos a 0
var capToZero = function (amount) {
    return amount < 0 ? 0 : amount;
};
// Retorna el % de peso correspondiente a una cantidad total
var getPercOfWeight = function (amount, perc, roundTo) {
    if (roundTo === void 0) { roundTo = 5; }
    return Math.ceil((amount * perc) / 100 / roundTo) * roundTo;
};
// Calcula el peso por serie para el calentamiento, si el peso objetivo es menor
// al peso inicial (peso de la barra más peso extra para alcanzar el minimo), no se retorna nada.
var calculateWeightWarmup = function (targetWeight, startingWeight, barWeight, minimunBarWeight) {
    if (startingWeight === void 0) { startingWeight = 0; }
    if (barWeight === void 0) { barWeight = 45; }
    if (minimunBarWeight === void 0) { minimunBarWeight = 45; }
    var extraWeight = capToZero(minimunBarWeight - barWeight);
    var even = (targetWeight - (barWeight + extraWeight)) / 4;
    var startingWithoutBarWeight = capToZero(startingWeight - (barWeight + extraWeight));
    var alternate = even >= 45 && even <= 50;
    var jump = even >= 90 ? 90 : even >= 45 ? 40 : even < 10 ? 10 : even;
    var series = jump === 90 ? 6 : 5;
    var result = [];
    var weight = startingWithoutBarWeight + barWeight + extraWeight;
    // no retorna una lista de pesos si el peso objetivo es menor al peso minimo
    // de la barra o los saltos son menores a 5 libras
    if (targetWeight <= weight) {
        return result;
    }
    for (var i = 0; i < series; i++) {
        var counter = i + 1;
        var addWeight = counter > 2;
        var addJump = alternate && (counter == 3 || counter == 5) ? jump + 10 : jump;
        if (addWeight) {
            weight = weight + addJump;
        }
        if (weight >= targetWeight)
            break;
        result.push({
            totalWeight: weight,
            warmupSet: counter,
            jump: addJump,
            barWeight: barWeight,
            extraBarWeight: extraWeight,
        });
    }
    return result;
};
exports.calculateWeightWarmup = calculateWeightWarmup;
var calculateWeightWarmupPerc = function (targetWeight, percentages, addEmptyBar, barWeight, minimumBarWeight) {
    if (percentages === void 0) { percentages = [45, 65, 85]; }
    if (addEmptyBar === void 0) { addEmptyBar = false; }
    if (barWeight === void 0) { barWeight = 45; }
    if (minimumBarWeight === void 0) { minimumBarWeight = 45; }
    var extraWeight = capToZero(minimumBarWeight - barWeight);
    var result = addEmptyBar
        ? [
            {
                warmupSet: 1,
                totalWeight: barWeight + extraWeight,
                jump: 0,
                barWeight: barWeight,
                extraBarWeight: extraWeight,
            },
            {
                warmupSet: 2,
                totalWeight: barWeight + extraWeight,
                jump: 0,
                barWeight: barWeight,
                extraBarWeight: extraWeight,
            },
        ]
        : [];
    for (var i = 0; i < percentages.length; i++) {
        var counter = addEmptyBar ? i + 3 : i + 1;
        result.push({
            totalWeight: getPercOfWeight(targetWeight, percentages[i]),
            warmupSet: counter,
            jump: percentages[i],
            barWeight: barWeight,
            extraBarWeight: extraWeight,
        });
    }
    return result;
};
exports.calculateWeightWarmupPerc = calculateWeightWarmupPerc;
var calculatePlatesLBS = function (plates, weight) {
    var platesAmounts = [];
    var halfWeight = weight / 2;
    plates.sort(function (a, b) { return 0 - (a > b ? 1 : -1); });
    for (var i = 0; i < plates.length; i++) {
        if (halfWeight <= 0) {
            break;
        }
        var plate = plates[i];
        var amount = Math.floor(halfWeight / plate);
        // resta peso segun la cantidad de discos obtenidos
        halfWeight -= plate * amount;
        if (amount > 0) {
            platesAmounts.push({ plate: plate, amount: amount });
        }
    }
    if (halfWeight > 0) {
        platesAmounts.push({ plate: halfWeight * 2, amount: 0 });
    }
    return platesAmounts;
};
exports.calculatePlatesLBS = calculatePlatesLBS;
//# sourceMappingURL=math.js.map