"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.calculatePlatesLBS = exports.calculateWeightWarmupPerc = exports.calculateWeightWarmup = void 0;
var math_1 = require("./math");
Object.defineProperty(exports, "calculateWeightWarmup", { enumerable: true, get: function () { return math_1.calculateWeightWarmup; } });
var math_2 = require("./math");
Object.defineProperty(exports, "calculateWeightWarmupPerc", { enumerable: true, get: function () { return math_2.calculateWeightWarmupPerc; } });
var math_3 = require("./math");
Object.defineProperty(exports, "calculatePlatesLBS", { enumerable: true, get: function () { return math_3.calculatePlatesLBS; } });
//# sourceMappingURL=index.js.map