import type { PlateLBS, WeightWarmup } from "./math.types";
export declare const calculateWeightWarmup: (targetWeight: number, startingWeight?: number, barWeight?: number, minimunBarWeight?: number) => WeightWarmup;
export declare const calculateWeightWarmupPerc: (targetWeight: number, percentages?: Array<number>, addEmptyBar?: boolean, barWeight?: number, minimumBarWeight?: number) => WeightWarmup;
export declare const calculatePlatesLBS: (plates: Array<number>, weight: number) => Array<PlateLBS>;
