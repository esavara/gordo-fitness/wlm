export type WeightWarmup = Array<{
	warmupSet: number;
	totalWeight: number;
	jump: number;
	barWeight: number;
	extraBarWeight: number;
}>;

export type PlateLBS = {
	plate: number;
	amount: number;
};
