/*
 * Contiene utilidades para calcular varios aspectos en una rutina para el
 * levantamiento de pesas
 * */

import type { PlateLBS, WeightWarmup } from "./math.types";

// Convierte valores negativos a 0
const capToZero = (amount: number): number => {
  return amount < 0 ? 0 : amount;
};

// Retorna el % de peso correspondiente a una cantidad total
const getPercOfWeight = (amount: number, perc: number, roundTo = 5): number => {
  return Math.ceil((amount * perc) / 100 / roundTo) * roundTo;
};

// Calcula el peso por serie para el calentamiento, si el peso objetivo es menor
// al peso inicial (peso de la barra más peso extra para alcanzar el minimo), no se retorna nada.
export const calculateWeightWarmup = (
  targetWeight: number,
  startingWeight = 0,
  barWeight = 45,
  minimunBarWeight = 45
): WeightWarmup => {
  const extraWeight = capToZero(minimunBarWeight - barWeight);
  const even = (targetWeight - (barWeight + extraWeight)) / 4;
  const startingWithoutBarWeight = capToZero(
    startingWeight - (barWeight + extraWeight)
  );
  const alternate = even >= 45 && even <= 50;
  const jump = even >= 90 ? 90 : even >= 45 ? 40 : even < 10 ? 10 : even;
  const series = jump === 90 ? 6 : 5;

  const result: WeightWarmup = [];
  let weight = startingWithoutBarWeight + barWeight + extraWeight;

  // no retorna una lista de pesos si el peso objetivo es menor al peso minimo
  // de la barra o los saltos son menores a 5 libras
  if (targetWeight <= weight) {
    return result;
  }

  for (let i = 0; i < series; i++) {
    const counter = i + 1;
    const addWeight = counter > 2;
    const addJump =
      alternate && (counter == 3 || counter == 5) ? jump + 10 : jump;

    if (addWeight) {
      weight = weight + addJump;
    }

    if (weight >= targetWeight) break;

    result.push({
      totalWeight: weight,
      warmupSet: counter,
      jump: addJump,
      barWeight: barWeight,
      extraBarWeight: extraWeight,
    });
  }

  return result;
};

export const calculateWeightWarmupPerc = (
  targetWeight: number,
  percentages: Array<number> = [45, 65, 85],
  addEmptyBar = false,
  barWeight = 45,
  minimumBarWeight = 45
): WeightWarmup => {
  const extraWeight = capToZero(minimumBarWeight - barWeight);

  const result: WeightWarmup = addEmptyBar
    ? [
        {
          warmupSet: 1,
          totalWeight: barWeight + extraWeight,
          jump: 0,
          barWeight: barWeight,
          extraBarWeight: extraWeight,
        },
        {
          warmupSet: 2,
          totalWeight: barWeight + extraWeight,
          jump: 0,
          barWeight: barWeight,
          extraBarWeight: extraWeight,
        },
      ]
    : [];

  for (let i = 0; i < percentages.length; i++) {
    const counter = addEmptyBar ? i + 3 : i + 1;

    result.push({
      totalWeight: getPercOfWeight(targetWeight, percentages[i]),
      warmupSet: counter,
      jump: percentages[i],
      barWeight: barWeight,
      extraBarWeight: extraWeight,
    });
  }

  return result;
};

export const calculatePlatesLBS = (
  plates: Array<number>,
  weight: number
): Array<PlateLBS> => {
  const platesAmounts: Array<PlateLBS> = [];
  let halfWeight = weight / 2;

  plates.sort((a, b) => 0 - (a > b ? 1 : -1));

  for (let i = 0; i < plates.length; i++) {
    if (halfWeight <= 0) {
      break;
    }
    const plate = plates[i];
    const amount = Math.floor(halfWeight / plate);

    // resta peso segun la cantidad de discos obtenidos
    halfWeight -= plate * amount;

    if (amount > 0) {
      platesAmounts.push({ plate, amount });
    }
  }

  if (halfWeight > 0) {
    platesAmounts.push({ plate: halfWeight * 2, amount: 0 });
  }

  return platesAmounts;
};
